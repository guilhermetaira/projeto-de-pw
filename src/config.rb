css_dir = '../library/css/'
sass_dir = '../assets/sass/'
images_dir = '../library/images/'
fonts_dir = '../library/fonts/'
javascripts_dir = '../library/js'
relative_assets = true
line_comments = false
output_style = :compressed
preferred_syntax = :scss

asset_cache_buster :none