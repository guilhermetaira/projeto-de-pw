var gulp			= require('gulp'),
	plumber			= require('gulp-plumber'),
	rename			= require('gulp-rename'),
	runSequence 	= require('run-sequence');
	args			= require('yargs').argv,
	gulpif			= require('gulp-if'),
	notify			= require('gulp-notify'),
	concat			= require('gulp-concat'),

	jade			= require('gulp-jade-php'),
	
	uglify			= require('gulp-uglify-es').default,

	imagemin		= require('gulp-imagemin'),

	sass			= require('gulp-sass'),
	autoprefixer	= require('gulp-autoprefixer'),

	isRelease = args.release || false;

	sass.compile	= require('node-sass');

// JADE
gulp.task('jade', function buildHTML() {
	return gulp.src(['../assets/jade/**/*.jade', '!../assets/jade/**/_*.jade'])
		.pipe(plumber({
			errorHandler: notify.onError("Error: <%= error.message %>")
		}))
		.pipe(jade({
			'pretty': (!isRelease) ? true : false,
			'locals': {
				'echo': function(str) {
					return "<?php echo " + str + " ?>"
				},
				'image': function(src) {
					return ("<?php echo get_template_directory_uri() ?>/library/images/" + src);
				},
				'background': function(src, fromWP) {
					var url = "/library/images/";
					url += src;
					if (fromWP) {
						return ("background-image:url('<?php echo " + src + "?>')");
					} else {
						return ("background-image:url('<?php echo get_template_directory_uri() . \"" + url + "\" ?>')");
					}
				},
				'css': function(value) {
					return ("<?php echo get_template_directory_uri() ?>/library/css/" + value);
				},
				'js': function(value) {
					return ("<?php echo get_template_directory_uri() ?>/library/js/" + value);
				},
				'library': function(src) {
					return ("<?php echo get_template_directory_uri() ?>/library/" + src);
				}
			}
		}))
		.pipe(plumber.stop())
		.pipe(gulp.dest("../"))
});

// IMAGES
gulp.task('images', function(){
	if (!isRelease) {
		console.warn("WARNING: running whitout --release, skipping image minification");
	}
	return gulp.src('../assets/images/**/*')
		.pipe((plumber({
			errorHandler: notify.onError("Error: <%= error.message %>")
		})))
		.pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
		.pipe(plumber.stop())
		.pipe(gulp.dest('../library/images/'));
});

// STYLES
gulp.task('styles', function(){
	gulp.src(['../assets/sass/**/*.scss'])
		.pipe(plumber({
			errorHandler: function (error) {
				console.log(error.message);
				this.emit('end');
		}}))
		.pipe(sass({
			outputStyle		: 'nested',
			includePaths	: [
				'./node_modules/compass-mixins/lib'
			]
		}))
		.pipe(plumber.stop())
		.pipe(autoprefixer({
			overrideBrowserslist : ['last 2 versions']
		}))
		.pipe(gulp.dest('../library/css/'))
});

// gulp.task('autoprefixer', function () {
// 	gulp.src(['../library/css/**/*.css'])
// 	.pipe(autoprefixer({
// 		overrideBrowserslist : ['last 2 versions']
// 	}))
// 	.pipe(gulp.dest('../library/css/'));
// });

gulp.task('js', function () {
	return gulp.src('../assets/js/**/*.js')
		.pipe(plumber({
			errorHandler: function (error) {
				console.log(error.message);
				this.emit('end');
		}}))
		.pipe(gulpif(isRelease, uglify()))
		.pipe(rename({suffix: '.min'}))
		.pipe(plumber.stop())
		.pipe(gulp.dest('../library/js/'))
});

gulp.task('json', function () {
	return gulp.src('../assets/json/**/*.json')
		.pipe(plumber({
			errorHandler: function (error) {
				console.log(error.message);
				this.emit('end');
		}}))
		.pipe(gulpif(isRelease, uglify()))
		.pipe(plumber.stop())
		.pipe(gulp.dest('../library/json/'))
});

gulp.task('fonts', function () {
	return gulp.src('../assets/fonts/**/*')
		.pipe(gulp.dest('../library/fonts/'))
});


gulp.task('build', function(){
	runSequence(['jade','js', 'styles', 'fonts', 'images', 'json']);
});

gulp.task('default', ['build']);

gulp.task('watch', function() {
	runSequence(['build']);
	gulp.watch("../assets/jade/**/*.jade", ['jade']);
	gulp.watch("../assets/sass/**/*.scss", ['styles']);
	gulp.watch("../assets/js/**/*.js", ['js']);
	gulp.watch("../assets/json/**/*.json", ['json']);
	gulp.watch("../assets/fonts/**/*", ['fonts']);
	gulp.watch("../assets/images/**/*", ['images']);
});