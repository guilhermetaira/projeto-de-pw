(function($) {
	return $.fn.validate = function(onSuccess, onError, submit) {
		
		let	$form	= $(this),
			errors	= 0,
			$inputs	= $('input[type=text], input[type=email], input[type=phone], select, input[type=password], input[type=url], input[type=tel], input[type=file], textarea', $form).not(".hidden"),
			
			isNull	= function(value) {
				if (value === "") {
					return true;
				}
				return false;
			},
			isEmail	= function(value) {
				var reg;
				if (!isNull(value)) {
					reg = new RegExp(/^[a-z][\w.-]+@\w[\w.-]+\.[\w.-]*[a-z][a-z]$/i);
					return reg.exec(value);
				}
				return false;
			},
			isURL	= function(value) {
				var reg;
				if (!isNull(value)) {
					reg = new RegExp(/[-a-zA-Z0-9@:%_\+.~#?&\/\/=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&\/\/=]*)?/gi);
					return reg.exec(value);
				}
				return false;
			};
		
		// Listeners
		$inputs.on('focus', function() {
			$(this).removeClass('success error');
		});
		
		$inputs.on( 'change', function() {
			$(this).removeClass('error');
		} )
		
		$form.on('submit', function(e) {
			$inputs	= $('input[type=text], input[type=email], input[type=phone], select, input[type=password], input[type=url], input[type=tel], input[type=file], textarea', $form).not(".hidden");
			errors = 0;
			$inputs.each(function() {
				let rule	= $(this).attr('data-rule'),
					value	= $(this).val(),
					error	= false;
				
				switch (rule) {
					case 'required':
						if ( isNull( value ) ) {
							errors++;
							error = true;
						}
						break;
					case 'email':
						if ( !isEmail( value ) ) {
							errors++;
							error = true;
						}
						break;
						
					case 'url':
						if ( !isURL( value ) ) {
							errors++;
							error = true;
						}
						break;
						
					case 'pdf':
						if (isNull ( value ) ) {
							errors++;
							error = true;
						} else {
							if ( value.split("\\").slice(-1)[0].split(".").splice(-1)[0] !== "pdf" ) {
								errors++;
								error = true;
							}
						}
						break;
				}
				
				if ( error )
					$(this).addClass('error').removeClass('success');
				else
					$(this).addClass('success').removeClass('error');
			});
			
			if (errors === 0) {
				if (typeof onSuccess === 'function') onSuccess();
				if (!submit) e.preventDefault();
			} else {
				if (typeof onError === 'function') onError();
				e.preventDefault();
			}
		});
	};
})(jQuery);