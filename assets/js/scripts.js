(function($) 
{
	$(document).ready(function() 
	{	
		/******************************
		HEADER
		*******************************/ 
		$('.hamburger').on('click', function()
		{
			$(this).toggleClass('is-active');
			$('#mobile-menu').toggleClass('open');
		});
	});
})(jQuery);